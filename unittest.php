<?php
include 'cricket.php';
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{

    public function testGetStrickResult()
    {
        $playerA = array('0' => 5,'1' => 30,'2' => 25,'3' => 10,'4' => 15,'5' => 1,'6' => 9,'OUT' => 5);
        $cricket = new Cricket;
        $this->assertTrue(in_array($cricket->getStrickResult($playerA), [0,1,2,3,4,5,6,'OUT']));
    }

    public function testGetStrickResultWithEmpty($playerA = null)
    {
        $cricket = new Cricket;
        $this->assertTrue(in_array($cricket->getStrickResult($playerA), [0,1,2,3,4,5,6,'OUT']));
    }

    public function testChangeStrick()
    {
        $cricket = new Cricket;
        $cricket->stricker = 'GURU';
        $cricket->nonStricker = 'ARUN';

        $cricket->changeStrick();

        $this->assertEquals('ARUN', $cricket->stricker);
        $this->assertEquals('GURU', $cricket->nonStricker);
    }

    public function testStorePlayersScore()
    {
        $test = array(
            array('name' => 'BharaniGuru', 'run' => 1),
            array('name' => 'BharaniGuru', 'run' =>6),
            array('name' => 'BharaniGuru', 'run' =>3),
            array('name' => 'BharaniGuru', 'run' =>4));
        $cricket = new Cricket;
        foreach ($test as $value) {
            $cricket->storePlayersScore($value['name'], $value['run']);
        }
        $this->assertEquals(14, $cricket->individualScrores['BharaniGuru']['runs']);
    }
}