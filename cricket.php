<?php
class Cricket 
{	
	function __construct()
	{
		$this->individualScrores =  array();
	}

	# =======================================================================================
	# =           Using random number, this function is used to get strick result           =
	# =======================================================================================
	
	function getStrickResult($stickPossibilities)
	{
		if(is_array($stickPossibilities)){
			$randomPercentage = rand(1,100);
			$count = 0;
			foreach ($stickPossibilities as $key => $value) {
				$count += $value;
				if ($count >= $randomPercentage) break;
			}
			return $key;	
		}
		return false;
	}

	# =======================================================================================
	# =           Below function is used to change stricker to non stricker           		=
	# =======================================================================================
	function changeStrick()
	{
		$tmp = $this->nonStricker;
		$this->nonStricker = $this->stricker;
		$this->stricker = $tmp;
	}

	# =======================================================================================
	# =   Below function is used to store individual players scores, balls and wickets		=
	# =======================================================================================

	function storePlayersScore($playerName, $runs, $wicket = FALSE)
	{
		if(!isset($this->individualScrores[$playerName])){
			$this->individualScrores[$playerName] = array('runs' => $runs, 'balls' => 1, 'wicket' => $wicket);
		}else{
			$this->individualScrores[$playerName]['runs'] += $runs;
			$this->individualScrores[$playerName]['balls']++;
			$this->individualScrores[$playerName]['wicket'] = $wicket;
		}
	}

	function getMatchResult($playersProbability, $oversLeft, $runsToWin)
	{

		$wickets = 0;
		$runs = 0;
		$win = false;
		$ballsLeft = $oversLeft*6;
		$wicketsLeft = count($playersProbability) - 1;

		$this->stricker = array_shift($playersProbability);
		$this->nonStricker = array_shift($playersProbability);

		$commentary[] = "<br><b>".$oversLeft." overs left. ".$runsToWin." runs to win</b>";
		
		for ($i=1; $i <= $oversLeft*6; $i++) { 
			$ballsLeft--;
			$strickValue = $this->getStrickResult($this->stricker['PLAYER_PROBABILITY']);
			
			if($strickValue === 'OUT'){
				$commentary[] = $this->stricker['PLAYER_NAME'] ." Got OUT"; 

				$this->storePlayersScore($this->stricker['PLAYER_NAME'], 0, true);

				$this->stricker = array_shift($playersProbability);
				$wickets++;
				
				if($wickets == $wicketsLeft){
					break;
				}
			}else{
				$commentary[] =  $this->stricker['PLAYER_NAME'] ." scrores " . $strickValue . (($strickValue == 0 || $strickValue == 1 ) ? " Run " : " Runs ");
				$runs += $strickValue;

				$this->storePlayersScore($this->stricker['PLAYER_NAME'], $strickValue);
			}

			if($strickValue !='OUT' && $strickValue % 2 != 0){
				$this->changeStrick();
			}

			if($runs >= $runsToWin){
				$win = true; break;
			}

			if($i % 6 == 0){
				$this->changeStrick();
				$commentary[] = "<br><b>".($oversLeft - $i/6)." overs left. ".($runsToWin - $runs)." runs to win</b>";
			}
		}

		if($win){
			$marchResult = "Bengaluru won by ".($wicketsLeft - $wickets)." wicket and ".$ballsLeft." balls remaining";
		}else{
			$marchResult = "Bengaluru lost by ".($runsToWin - $runs)." runs and ".$ballsLeft." balls remaining";
		}

		return array('commentary' => $commentary, 'individual_scrores' => $this->individualScrores, 'march_result' => $marchResult);
	}
}
